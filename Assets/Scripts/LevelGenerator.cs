﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    public GameObject[] roomPrefabs; // Array of room prefabs to use for generating the level
    public int minNumberOfRooms; // Minimum number of rooms to generate in the level
    public int maxNumberOfRooms; // Maximum number of rooms to generate in the level
    public float minRoomWidth; // Minimum width of each room
    public float maxRoomWidth; // Maximum width of each room
    public float minRoomHeight; // Minimum height of each room
    public float maxRoomHeight; // Maximum height of each room

    private List<GameObject> spawnedRooms = new List<GameObject>(); // List of spawned rooms

    void Start()
    {
        GenerateLevel(); // Call the GenerateLevel method to generate the level
    }

    public void GenerateLevel()
    {
        // Generate a random number of rooms within the given range
        int numberOfRooms = Random.Range(minNumberOfRooms, maxNumberOfRooms + 1);

        // Spawn the first room at the starting position (0, 0)
        GameObject firstRoom = Instantiate(roomPrefabs[0], new Vector3(0, 0, 0), Quaternion.identity);
        spawnedRooms.Add(firstRoom);

        // Generate the remaining rooms in the level
        for (int i = 1; i < numberOfRooms; i++)
        {
            // Choose a random room prefab from the array
            int randomIndex = Random.Range(0, roomPrefabs.Length);
            GameObject roomPrefab = roomPrefabs[randomIndex];

            // Generate random width and height values for the room
            float roomWidth = Random.Range(minRoomWidth, maxRoomWidth);
            float roomHeight = Random.Range(minRoomHeight, maxRoomHeight);

            // Spawn the room at a random position near the last spawned room
            Vector3 spawnPosition = spawnedRooms[i-1].transform.position + new Vector3(roomWidth, 0, 0);
            GameObject room = Instantiate(roomPrefab, spawnPosition, Quaternion.identity);
            spawnedRooms.Add(room);
        }
    }
}






