﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Gun : MonoBehaviour
{
    public float damage = 10f;
    public float range = 100f;
    public float reloadTime = 3f;
    public float recoilTime = 0.35f;
    static float ammoMag = 8f;
    static float totalAmmo = 20f;
    static float ammoNeeded = 0f;
    static bool isReloading = false;
    static bool isRecoiling = false;
    public Vector3 gunRecoil;
    private Vector3 defaultRotation;
    public Camera fpsCam;
    public ParticleSystem muzzleFlash;
    public AudioSource gunSound;
    public AudioSource gunClick;
    public AudioSource gunReload;
    public TextMeshProUGUI displayAmmo;
    public TextMeshProUGUI displayReloading;
    public GameObject giveAmmo;


    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            if (isReloading == false)
            {
                Shoot();
            }
            else if (isReloading == true)
            {
                print("You are reloading.");
            }
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            if (ammoMag == 8f)
            {
                print("Reloading is not necessary");
            }
            else
            {
                Reload();
            }
        }
        if (giveAmmo.activeInHierarchy)
        {
            totalAmmo += 24;
            giveAmmo.SetActive(false);
        }
        if ((ammoMag <= 0f) && (totalAmmo >= 0))
        {
            Reload();
        }
        if ((isReloading == true) && (reloadTime > 0f)) 
        {
            reloadTime -= Time.deltaTime;
        }
        if ((isRecoiling == true) && (recoilTime > 0f))
        {
            recoilTime -= Time.deltaTime;
        }
        if (reloadTime <= 0f)
        {
            isReloading = false;
            gunReload.Play();
            displayAmmo.text = ammoMag + "/" + totalAmmo;
            displayReloading.text = "";
            reloadTime = 3f;
        }
        if ((recoilTime <= 0))
        {
            isRecoiling = false;
        }
        if ((isRecoiling == false) && (recoilTime <= 0))
        {
            StopRecoil();
        }
        displayAmmo.text = ammoMag + "/" + totalAmmo;



    }

    void Start()
    {
        defaultRotation = transform.localEulerAngles;
        reloadTime = 3f;
        isReloading = false;
        displayAmmo.text = "8/20";
    }

    void Shoot()
    {
        if (ammoMag < 1)
        {
            gunClick.Play();
        }
        else
        {
            isRecoiling = true;
            recoilTime = 0.45f;
            ammoMag -= 1f;
            displayAmmo.text = ammoMag + "/" + totalAmmo;
            muzzleFlash.Play();
            gunSound.Play();
            Recoil();
            RaycastHit hit;
            if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range))
            {
                Debug.Log(hit.transform.name);
                Target target = hit.transform.GetComponent<Target>();
                if (target != null)
                {
                    target.TakeDamage(damage);
                }
            }
        }
    }
    void Reload()
    {
        displayReloading.text = "Reloading";
        if (totalAmmo > 8f)
        {
            isReloading = true;
            if (ammoMag < 8)
            {
                totalAmmo = totalAmmo - (8 - ammoMag);
                ammoMag = 8f;
            }
            else
            {
                totalAmmo = totalAmmo - 8f;
                ammoMag = 8f;
            }
        }
        else if (totalAmmo > 0f)
        {
            isReloading = true;
            ammoNeeded = 8 - ammoMag;
            if (ammoNeeded > totalAmmo)
            {
                ammoMag = ammoMag + totalAmmo;
                totalAmmo = 0;
            }
            else
            {
                ammoMag = 8f;
                totalAmmo = totalAmmo - ammoNeeded;
            }


        }
        else
        {
            print("No more ammo.");
        }
    }
    private void Recoil()
    {
        transform.localEulerAngles += gunRecoil;

    }
    private void StopRecoil()
    {       
        transform.localEulerAngles = defaultRotation;
    }
    public void addAmmo()
    {
        totalAmmo += 24f;
    }
}
