﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinGame : MonoBehaviour
{
    public GameObject winText;
    // Start is called before the first frame update
    void Start()
    {
        winText.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        winText.SetActive(true);
        Invoke("WinRestart", 5);
    }

    void WinRestart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
