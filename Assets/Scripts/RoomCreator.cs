﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomCreator : MonoBehaviour
{
    public GameObject roomPrefab; // The room prefab to use for creating new rooms
    public int numberOfRoomsToCreate; // The number of rooms to create
    public float roomWidth; // The width of each room
    public float roomHeight; // The height of each room

    private List<GameObject> createdRooms = new List<GameObject>(); // List of created rooms

    void Start()
    {
        for (int i = 0; i < numberOfRoomsToCreate; i++)
        {
            // Create a new room and add it to the createdRooms list
            GameObject room = CreateRoom();
            createdRooms.Add(room);

            // Add the new room prefab to the roomPrefabs array in the LevelGenerator script
            LevelGenerator levelGenerator = FindObjectOfType<LevelGenerator>();
            levelGenerator.roomPrefabs = createdRooms.ToArray();
        }
    }

    GameObject CreateRoom()
    {
        // Create a new room game object and add a BoxCollider component to it
        GameObject room = new GameObject("Room");
        BoxCollider boxCollider = room.AddComponent<BoxCollider>();
        boxCollider.size = new Vector3(roomWidth, roomHeight, 1);

        // Create a new mesh filter and mesh renderer for the room
        MeshFilter meshFilter = room.AddComponent<MeshFilter>();
        MeshRenderer meshRenderer = room.AddComponent<MeshRenderer>();

        // Create a new mesh for the room
        Mesh mesh = new Mesh();
        mesh.vertices = new Vector3[]
        {
            new Vector3(-roomWidth/2, -roomHeight/2, 0),
            new Vector3(-roomWidth/2, roomHeight/2, 0),
            new Vector3(roomWidth/2, roomHeight/2, 0),
            new Vector3(roomWidth/2, -roomHeight/2, 0)
        };
        mesh.triangles = new int[] { 0, 1, 2, 0, 2, 3 };
        mesh.uv = new Vector2[]
        {
            new Vector2(0, 0),
            new Vector2(0, 1),
            new Vector2(1, 1),
            new Vector2(1, 0)
        };
        meshFilter.mesh = mesh;

        // Set the material for the room renderer
        meshRenderer.material = new Material(Shader.Find("Standard"));

        return room;
    }
}
