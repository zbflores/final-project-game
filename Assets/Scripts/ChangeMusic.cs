﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMusic : MonoBehaviour
{
    public GameObject player;
    public AudioSource oldMusic;
    public AudioSource newMusic;
    public float doneOnce;
    // Start is called before the first frame update
    void Start()
    {
        doneOnce = 0f;  
    }

    // Update is called once per frame
    void OnTriggerEnter(Collider player)
    {
        if (doneOnce == 0f)
        {
            doneOnce = 1;
            oldMusic.Stop();
            newMusic.Play();
        }
    }
}
