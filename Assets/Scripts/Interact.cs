﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interact : MonoBehaviour
{
    public GameObject zone;
    public GameObject player;
    public GameObject leverOff;
    public GameObject leverOn;
    public GameObject light_glow;
    public GameObject moving_platform;
    public Vector3[] points;
    private Vector3 current_target;
    public int point_number = 0;
    public float speed;
    public float tolerance;
    private float delay_time;
    public float waypointPause;
    private float savePause;
    public float pauseCalled;
    public float delay_start;
    public bool automatic;
    public bool used = false;
    public bool activatable = false;
    void OnTriggerEnter(Collider player)  
    {
        activatable = true;

    }
    void OnTriggerExit(Collider player)
    {
        activatable = false;
    }

    void MovePlatform()
    {
        Vector3 heading = current_target - moving_platform.transform.position;
        print("Platform should be moving");
        moving_platform.transform.position += (heading / heading.magnitude) * speed * Time.deltaTime;
        if (heading.magnitude < tolerance)
        {
            moving_platform.transform.position = current_target;
            delay_start = Time.deltaTime;

        }
    }

    void UpdateTarget()
    {
        if (automatic)
        {
            waypointPause -= Time.deltaTime;
            print(waypointPause);
            print(pauseCalled);
            if (waypointPause <= 0.1f)
            {
                print("Delay has ended.");
                NextPlatform();
            }
        }
    }

    public void NextPlatform()
    {
        pauseCalled = 0f;
        print("Next platform called");
        point_number++;
        if (point_number >= points.Length)
        {
            print("Platform points reset to first target");
            point_number = 0;
        }
        current_target = points[point_number];
    }

	void Start()
	{
        savePause = waypointPause;
        pauseCalled = 0f;
        if (points.Length > 0)
        {
            current_target = points[0];
        }
        tolerance = speed * Time.deltaTime;

	}
	void Update()
    {
        if (used == true)
        {
            if (moving_platform.transform.position != current_target)
            {
                MovePlatform();
            }
            else
            {
                if (pauseCalled == 0f)
                    waypointPause = savePause;
                    pauseCalled = 1f;
                print("Updating platform target");
                UpdateTarget();
            }
        }
        if (activatable == true)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (used == false)
                {
                    used = true;
                    light_glow.SetActive(true);
                    leverOff.SetActive(false);
                    leverOn.SetActive(true);
                    print("Lever On");
                }
                else if (used == true)
                {
                    used = false;
                    light_glow.SetActive(false);
                    leverOn.SetActive(false);
                    leverOff.SetActive(true);
                    print("Lever Off");
                }
            }
        }
    }
}
