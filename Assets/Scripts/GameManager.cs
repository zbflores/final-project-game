﻿using UnityEngine;

public class GameManager : MonoBehaviour
{
    public LevelGenerator levelGenerator; // Drag and drop the LevelGenerator object here

    void Start()
    {
        levelGenerator.GenerateLevel(); // Call the GenerateLevel method to generate the level
    }
}
