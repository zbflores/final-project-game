﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    public bool isHealth = false;
    public GameObject giveAmmo;
    public GameObject giveHealth;
    public GameObject self;
    public GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("ActivatePickUp", 12);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter(Collider player)
    {
        if (isHealth == false)
        {
            giveAmmo.SetActive(true);
        }
        else if (isHealth == true)
        {
            giveHealth.SetActive(true);
        }
        Destroy(self);
    }
    void ActivatePickUp()
    {
        self.SetActive(true);
    }
}
