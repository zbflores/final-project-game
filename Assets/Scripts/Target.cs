﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Target : MonoBehaviour
{
    public float health = 50f;
    public float gameDone = 0f;
    private bool firstFall = true;
    public bool isPlayer = false;
    public bool safeFall = true;
    public GameObject giveHealth;
    public AudioSource ouch;
    public AudioSource gameOver;
    public AudioSource notification;
    public AudioSource fallWarning;
    public Vector3 velocity;
    public Rigidbody rigidBody;
    public Slider healthDisplay;
    public double _decelerationTolerance = 18.0;


    void Start()
    {
        firstFall = true;
        safeFall = true;
        rigidBody = GetComponent<Rigidbody>();
    }
    void FixedUpdate()
    {
        safeFall = Vector3.Distance(rigidBody.velocity, velocity) < _decelerationTolerance;
        velocity = rigidBody.velocity;
    }
    void Update()
    {
        if (safeFall == false)
        {
            ouch.Play();
            TakeDamage(25);
            safeFall = true;
            if (health <= 0f)
            {
                Death();
            }
        }
        if (isPlayer == true)
        {
            healthDisplay.value = health;
        }
        if (giveHealth.activeInHierarchy && isPlayer == true)
        {
            print("Giving Player Health");
            giveHealth.SetActive(false);
            health += 75;
            if (health > 100)
            {
                health = 100;
            }
        }    
    }
  
    public void TakeDamage(float amount)
    {
        ouch.Play();
        health -= amount;
        if (health <= 0f)
        {
            Death();
        }
        if (firstFall == true)
        {
            firstFall = false;
            notification.Play();
            Invoke("fallMessage", 1.5f);
        }
    }
    void Death()
    {
        if (isPlayer == false)
        {
            Destroy(gameObject);
        }
        else if (isPlayer == true && gameDone != 1f)
        {
            gameDone = 1f;
            gameOver.Play();
            print("Game Over.");
            Time.timeScale = 0.5f;
            Invoke("Restart", 2);

        }
    }
    void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    void fallMessage()
    {
        fallWarning.Play();
    }

}
