﻿using UnityEngine;
using System.Collections;

public class Tracking : MonoBehaviour
{
    public float speed = 2.5f;
    public GameObject m_target = null;
    public GameObject haveTarget;
    public GameObject isFacing;
    public GameObject turretBarrel;
    private bool track;
    Vector3 m_lastKnownPosition = Vector3.zero;
    Quaternion m_lookAtRotation;

    // Update is called once per frame
    void Update()
    {
        if (haveTarget.activeInHierarchy)
            track = true;
        else
            track = false;
        if (m_target && track == true)
        {
            if (m_lastKnownPosition != m_target.transform.position)
            {
                m_lastKnownPosition = m_target.transform.position;
                m_lookAtRotation = Quaternion.LookRotation(m_lastKnownPosition - transform.position);
            }

            if (transform.rotation != m_lookAtRotation)
            {
                transform.rotation = Quaternion.RotateTowards(transform.rotation, m_lookAtRotation, speed * Time.deltaTime);
            }
            if (transform.rotation == m_lookAtRotation)
            {
                isFacing.SetActive(true);
            }
            else
            {
                isFacing.SetActive(false);
            }
        }
    }

    bool SetTarget(GameObject target)
    {
        if (!target)
        {
            return false;
        }

        m_target = target;

        return true;
    }
}
