﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretAggro : MonoBehaviour
{
    public GameObject player;
    public GameObject haveTarget;
    public GameObject turretOnLight;
    public AudioSource turretReady;
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider player)
    {
        haveTarget.SetActive(true);
        turretReady.Play();
        turretOnLight.SetActive(true);

    }
    private void OnTriggerExit(Collider player)
    {
        haveTarget.SetActive(false);
        turretOnLight.SetActive(false);
    }
}
