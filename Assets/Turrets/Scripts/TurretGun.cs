﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretGun : MonoBehaviour
{
    public float range;
    public float damage;
    public float shootTimer;
    public float glowTimer;
    public GameObject turretBarrel;
    public GameObject blastGlow;
    public GameObject isFacing;
    public AudioSource turretFire;
    public ParticleSystem turretParticle;
    // Start is called before the first frame update
    void Start()
    {
        blastGlow.SetActive(false);
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isFacing.activeInHierarchy && shootTimer <= 0f)
        {
            Shoot();
            shootTimer = 1.75f;
            glowTimer = 0.5f;
        }
        if (glowTimer <= 0)
        {
            blastGlow.SetActive(false);
        }
        glowTimer -= Time.deltaTime;
        shootTimer -= Time.deltaTime;
    }
    public void Shoot()
    {

        RaycastHit hit;
        if (Physics.Raycast(turretBarrel.transform.position, turretBarrel.transform.forward, out hit, range))
        {
            Debug.Log(hit.transform.name);
            Target target = hit.transform.GetComponent<Target>();
            if (target != null)
            {
                turretFire.Play();
                turretParticle.Play();
                blastGlow.SetActive(true);
                target.TakeDamage(damage);
            }
        }
    }
}
